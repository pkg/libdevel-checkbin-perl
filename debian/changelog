libdevel-checkbin-perl (0.04-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 11:37:35 +0000

libdevel-checkbin-perl (0.04-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libdevel-checkbin-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 10:40:57 +0100

libdevel-checkbin-perl (0.04-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl and
      perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:50:20 +0100

libdevel-checkbin-perl (0.04-1.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 15:14:27 +0000

libdevel-checkbin-perl (0.04-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 29 Dec 2020 03:21:18 +0100

libdevel-checkbin-perl (0.04-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 00:50:09 +0000

libdevel-checkbin-perl (0.04-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.04
  * Set debhelper >= 9

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 01 Aug 2015 20:17:00 -0300

libdevel-checkbin-perl (0.03-1) unstable; urgency=medium

  * Import upstream version 0.03
  * Update years of packaging copyright.
  * Update build dependencies.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 May 2015 23:39:57 +0200

libdevel-checkbin-perl (0.02-1) unstable; urgency=low

  * Initial release (closes: #764764).

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Oct 2014 23:50:34 +0200
